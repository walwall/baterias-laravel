@extends('templates.base')

@section('conteudo')
    <main>
        <h1>CONCLUSÕES</h1>
        <hr>
       
        <p>
        A conclusão feita, foi que, "a vida útil" de cada pilha ou bateria está associado com a tensão com carga relativamente à tensão nominal, assim como, o valor da respectiva resistência interna.
        Com tudo, aprendemos a calcular a resistência interna de uma pilha/bateria, utilizando dados calculados por um multímetro e associando às determiadas tensões de cada um. 
        Para o desenvolvimento do trabalho, tivemos que aplicar conhecimentos relacionados à HTML para a criação do site, em Eletroeletrônica para as análises no Multímetro e os cálculos realizados para saber resistência, tensão e etc...<br>
        À partir de um trabalho sobre baterias em geral, cada mebro em específico teve uma função condizente com sua preferência. Tais como, André, que manteve a parte de javascript atualizada corretamente, e que inclusive foi o “owner” de todo o projeto, nos guiando dentre todos os tipos de códigos que precisamos utilizar.
Diogo, que foi o principal desenvolvedor do código html, se manteve centrado em fazer tudo que o codigo necessitava, contando também com a ajuda de todos os membros.
Walmir, que participou principalmente das medições que envolviam os 10 tipos de bateria/pilha.
E por último, mas não menos importante, o Luiz, que participou da criação do código em css, fazendo o nosso código principal ter uma organização necessária.


        </p>
    </main>
    @endsection

@section('rodape')
<h4>Rodapé da página conclusões</h4>
@endsection