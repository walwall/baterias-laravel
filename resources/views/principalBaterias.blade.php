@extends('templates.base')

@section('conteudo')
  
  <main>
    <h1>Turma: 2D1 - Grupo 2</h1>
    <h2>Professor:Ronaldo Silva Trindade</h2>
    <h3>Participantes:</h3>
    
    <hr>
  
  <table class="table table-striped">
    
    <tr>
        <td> MATRICULA </td>
        <td> NOME </td>
        <td> FUNÇÃO </td>
        
    </tr>
    <tr>
        <td> 0073411 </td>
        <td> Diogo Morais</td>
        <td> Desenvolvedor de HTML/CSS</td>
        
    </tr>
    <tr>
        <td> 0072553</td>
        <td> Walmir de Jesus</td>
        <td> Medições</td>
        
    </tr>
    <tr>
        <td> 0065648</td>
        <td> Andre Vieira</td>
        <td>  Gerente</td>
        
    </tr>
    <tr>
        <td> 0073608</td>
        <td> Luiz Felipe</td>
        <td> Desenvolvedor de JS</td>
        
    </tr>
    
    <img src="../trabeletro/pics/Baterias/IMG_0726.PNG" class="grupo">
    
  </table>
</main>
@endsection

@section('rodape')
<h4>Rodapé da página principal</h4>
@endsection