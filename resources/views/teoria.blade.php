@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Teoria</h1>
        <hr>
        <h2>Descrição das pilhas:</h2>
        <p>  
            As baterias recarregáveis ​​são dispositivos eletroquímicos que podem ser carregados e descarregados várias vezes, permitindo o uso prolongado em comparação com as baterias descartáveis. Essas baterias são uma opção mais econômica e sustentável, pois atendem a necessidade de substituição frequente e descarte de baterias usadas. <br>
            <br>
            <img src="../pics/Baterias/energy elgin.jpg" > <br>
            <br>
            <br>
            Uma pilha recarregável, também conhecida como bateria recarregável, é um dispositivo que pode ser recarregado após o esgotamento de sua carga. Ao contrário das pilhas descartáveis ​​tradicionais, as pilhas recarregáveis ​​​​são projetadas para serem usadas repetidamente, o que as torna uma opção mais econômica e ambientalmente amigável a longo prazo. <br>
            <br>
            <img src="../pics/Baterias/J.Y.X.jpg" >  <br>
              <br>
            Uma pilha não recarregável, também conhecida como pilha descartável ou pilha primária, é um dispositivo eletroquímico que produz energia por meio de uma reação química e não pode ser recarregada. Elas são projetadas para serem usadas uma única vez e, quando a energia é esgotada, devem ser descartadas como prioritárias.<br>
            
As pilhas não recarregáveis ​​são comumente encontradas em diferentes tamanhos e formatos, como pilhas alcalinas (AA, AAA, C, D, etc.) e pilhas de zinco-carbono. Elas são amplamente utilizadas em dispositivos eletrônicos de uso cotidiano, como controles remotos, lanternas, brinquedos, relógios, entre outros <br>
<br>
            <img src="../pics/Baterias/duracell AA.jpg" width="300px" > <br>
            <br>
            
            
            As baterias não recarregáveis, também conhecidas como baterias descartáveis, são projetadas para fornecer energia por um determinado período de tempo e, uma vez esgotadas, não podem ser recarregadas novamente. Elas são geralmente compostas por materiais químicos que reagem para gerar eletricidade, mas essa reação é irreversível, o que significa que a bateria não pode ser recarregada.
Existem diferentes tipos de baterias não recarregáveis, cada uma com suas próprias características e aplicações. <br>
            <img src="../pics/Baterias/golite.jpg" >  <br>
        </p>
    </main>
    @endsection

    @section('rodape')
    <h4>Rodapé da página teoria</h4>
    @endsection