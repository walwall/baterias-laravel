<?php

use App\Http\Controllers\PrincipalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',  [PrincipalController::class, 'principalBaterias'])->name('/');
Route::get('/teoria',  [PrincipalController::class, 'teoria'])->name('teoria');
Route::get('/procedimento',  [PrincipalController::class, 'procedimento'])->name('procedimentos');
Route::get('/medicoes',  [PrincipalController::class, 'medicoes'])->name('medicoes');
Route::get('/conclusoes',  [PrincipalController::class, 'conclusoes'])->name('conclusoes');

Route::get('/principal',  [PrincipalController::class, 'principal']);

Route::get('/pagina1', [PrincipalController::class, 'pagina1'] );

Route::get('/pagina2', [PrincipalController::class, 'pagina2'] );


