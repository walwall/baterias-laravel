git config --global use.name "progaut"
git config --global user.email "progaut.codaaut@gmail.com"

composer update
php artisan migrate:refresh --seed
php artisan serve